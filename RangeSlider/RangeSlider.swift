//
//  RangeSlider.swift
//  RangeSlider
//
//  Created by Tobias Ednersson on 24/02/16.
//
//

import UIKit

class RangeSlider: UIControl {

    var delegate:RangeSliderDelegate? = nil
    var maxValue:Float
    var minValue:Float
    var curentMin:Float
    var currentMax:Float
    
    //maxslider knob
    let knobLayer = CALayer()
    
    
    // minslider knob
    let lowerknobLayer = CALayer()
    let trackLayer = CALayer()
    let rangeLayer = CALayer()
    var lowerSelected:Bool = false;
    var higherSelected:Bool = false;
    var knobsize:CGFloat
    
    /* sliderlength - size of knob */
    var usableSlider:CGFloat
    
    
    
    
    
    override  init(frame: CGRect) {
        

        
        
        self.maxValue = 10
        self.minValue = 0
        
        self.currentMax = self.maxValue
        self.curentMin = self.minValue
        self.knobsize = frame.height
        
        //The entire knob must always be visible.
        // so it can not slide it's center over the entire view
        self.usableSlider = frame.width - knobsize
        
        super.init(frame: frame);
        self.rangeLayer.backgroundColor = UIColor.blueColor().CGColor
        self.layer.addSublayer(rangeLayer)
        trackLayer.backgroundColor = UIColor.redColor().CGColor
        self.layer.addSublayer(trackLayer)
        knobLayer.backgroundColor = UIColor.greenColor().CGColor
        lowerknobLayer.backgroundColor = UIColor.yellowColor().CGColor
        self.layer.addSublayer(lowerknobLayer)
        self.layer.addSublayer(knobLayer)
       
        self.addLayerFrames()
         self.imageOnKnobs()
           }

    
    func addLayerFrames() ->Void {
        
        //blur to indicate knob has been selected
        if(lowerSelected) {
        
            self.lowerknobLayer.opacity = 0.5
            //lowerknobLayer.removeFromSuperlayer()
            //self.layer.insertSublayer(self.lowerknobLayer, above: self.knobLayer)
        }
        
        else  {
            self.lowerknobLayer.opacity = 1.0
        }
            
        if(higherSelected) {
            self.knobLayer.opacity = 0.5
          //  self.knobLayer.removeFromSuperlayer()
            //self.layer.insertSublayer(knobLayer, above: lowerknobLayer)
        }
        
        else {
            self.knobLayer.opacity = 1.0
        }
        
        
        
        
        let lowerPos = valueToPosition(self.curentMin)
        self.lowerknobLayer.frame = CGRectMake(lowerPos - self.knobsize/2.0, 0, self.knobsize, self.knobsize)
     
        self.lowerknobLayer.setNeedsDisplay()
        
       
        
        let higherPos = valueToPosition(self.currentMax);
        
        self.knobLayer.frame = CGRectMake(higherPos - self.knobsize/2.0,0, self.knobsize, self.knobsize)
        self.knobLayer.setNeedsDisplay()
        
        
        let range = higherPos - lowerPos
        rangeLayer.frame = CGRectMake(lowerPos + self.knobsize/2, self.knobsize/3, range, self.knobsize/3.0)
        rangeLayer.setNeedsDisplay()
        self.imageOnKnobs()
        
        
        
        
    }
    
    /*Converts a value on the slider to the proper x-position for the corresponding knob */
    func valueToPosition(value:Float) ->CGFloat {

      var position =   ((CGFloat(value - self.minValue)) / CGFloat(maxValue - self.minValue)) * self.usableSlider
      position = position + self.knobsize/2
        return position
        
        
        

    }
    
    
    private func imageOnKnobs() ->Void {
        let image = UIImage(named:"knob.png")
        //knobLayer.contents = UIImage(named: "knob.png")?.CGImage
        knobLayer.contents = image!.CGImage
        
        
        knobLayer.borderColor = UIColor.blackColor().CGColor;
        knobLayer.borderWidth = 2.0;
        lowerknobLayer.borderColor = UIColor.blackColor().CGColor
        lowerknobLayer.borderWidth = 2.0
        lowerknobLayer.contents = image!.CGImage
    }
    
    
    convenience init(maxValue:Float,minValue:Float,frame:CGRect) {
        self.init(frame:frame)
        self.minValue = minValue
        self.maxValue = maxValue
        
        
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        let touchpoint = touch.locationInView(self)
        
        //check if either knob is touched
        if(CGRectContainsPoint(lowerknobLayer.frame, touchpoint)) {
            self.lowerSelected = true;
        }
        else if(CGRectContainsPoint(self.knobLayer.frame, touchpoint)) {
            self.higherSelected = true;
        }
        //recalculate and redraw the frames
        self.addLayerFrames()
        return self.higherSelected || self.lowerSelected
        
    }
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        let touchpoint = touch.locationInView(self)
    
        let posX = touchpoint.x
        
        
        let distanceTraveled = (self.maxValue - self.minValue) * Float(posX/self.usableSlider)
        
        
        //modify lowervalue if lower is selected
        if(lowerSelected) {
        self.curentMin = self.minValue + distanceTraveled
        }
        
        
        //...and vice versa
        else if(higherSelected) {
            self.currentMax = self.minValue + distanceTraveled
        }
        
       
        
        // Bound the values downwards and upwards, so that they 
        // are never lower than min or higher than max.
        //So we cannot slide the knobs outside the slider
        if(self.curentMin > self.currentMax) {
            
            self.currentMax = self.curentMin
            
        }

        if(self.currentMax > self.maxValue) {
            self.currentMax = self.maxValue
        }
        
        if(self.currentMax < self.minValue) {
            self.currentMax = self.minValue
        }
        
        if(self.curentMin < self.minValue) {
            self.curentMin = self.minValue
        }
        
        
        //if at any time the current maxvalue becomes greater than the min value, correct this.
        if(self.curentMin > self.maxValue) {
            self.curentMin = self.maxValue
        }
        
        
        
        if(self.delegate != nil) {
            delegate!.RangeSliderDidChangeValue(self.currentMax - self.curentMin)
            
        }
        
        self.addLayerFrames()
        return true;
    }
    
    
    override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
    
    //reset knobs to untouched state
    self.higherSelected = false
    self.lowerSelected = false
    // redraw the layers
    self.addLayerFrames()
    }
    
    
   }
