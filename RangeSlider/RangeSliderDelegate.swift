//
//  RangeSliderDelegate.swift
//  RangeSlider
//
//  Created by Tobias Ednersson on 24/02/16.
//
//

import Foundation

public protocol RangeSliderDelegate {
    
     func RangeSliderDidChangeValue(range:Float) ->Void;
}