//
//  ViewController.swift
//  RangeSlider
//
//  Created by Tobias Ednersson on 24/02/16.
//
//

import UIKit

class ViewController: UIViewController,RangeSliderDelegate {
    var currentBounce:Float = 0

    
    @IBOutlet weak var ball: UIImageView!
    var _slider:RangeSlider? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let margin:CGFloat = 20.0;
        let width = self.view.frame.size.width - (margin * 2)
        let height = self.view.frame.size.height/20
        let sliderFrame = CGRectMake(margin, margin, width, height)
        
        _slider = RangeSlider(maxValue: 1, minValue: 0, frame: sliderFrame)
        _slider?.backgroundColor = UIColor.redColor()
        self.view.addSubview(_slider!)
        _slider?.delegate = self
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
    
    }
    
    
    
    private func runAnimation() {
        
        let origpos = self.ball.center
        let distance_to_bottom = self.view.frame.height - self.ball.center.y
        
        
        
        let usableheight = self.view.frame.height - self.ball.frame.height/2 - distance_to_bottom
        
        
        
        UIView.animateKeyframesWithDuration(2.0 * Double(self.currentBounce), delay: 0.0, options:[] , animations:{
            UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 0.5, animations: {
                //let usable_height = self.view.frame.height - (self.ball.frame.height/2) - (self.ball.center.y)
                self.ball.center =   CGPointMake(self.ball.center.x,self.ball.center.y - usableheight * CGFloat(self.currentBounce))
                
                
                
            })
            
            
            
            UIView.addKeyframeWithRelativeStartTime(0.5, relativeDuration: 0.5, animations: {
                
                self.ball.center = origpos
                
                
            })
            
            
            
            }, completion: nil
        
        )
        

    }
    
    
    
    
    
    
    
    
    

    func RangeSliderDidChangeValue(range: Float) {
        self.currentBounce = range;
        self.runAnimation()
        
        print(self.currentBounce)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

